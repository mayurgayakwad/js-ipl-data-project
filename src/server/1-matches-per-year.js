// below line is use for fileSystem which is require for our project
const fs = require('fs');

// below line is contains a data of matches 
let matchesObj = require('../data/matches')
// console.log(data)

//below line using reduce to find out our desire output like {  season : NumberOfMatchesPlayedInGiveSeason} => like { '2008' : 123 somthing}

const matchesPerYear = matchesObj.reduce((obj , curr) => {
    if (obj[curr.season]) {
        obj[curr.season] += 1

    } else {
        obj[curr.season] = 1
    }
    return obj


},{})

// above logic is so simpled where is impletement if else condition using reduce 
// reduce iterate over the matches data after obj[curr.season] find whether is present or not if not then it will put as key and value pair
// if present then it add only season count


//console.log(matchesPerYear)


//below line is use for converting our calculated data into stringfy format
//after that converted stringify to json file... there use of filesystem

const result = JSON.stringify(matchesPerYear,'  ', '     ')
fs.writeFileSync('../public/1-matches-per-year.json',result)



