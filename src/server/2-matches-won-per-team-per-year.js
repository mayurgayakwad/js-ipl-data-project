//matches-won-per-team-per-year

const fs = require ('fs');
let matchesObj = require('../data/matches')

//below reduce methode iterate over the matches data to find season and matches winner 
const matchesWonPerYear = matchesObj.reduce((obj , currObj) => { 
    if(obj[currObj["winner"]]){
        if(obj[currObj.winner][currObj.season]){
            obj[currObj.winner][currObj.season]+=1;
        }
        else{
            obj[currObj.winner][currObj.season] = 1;
        }
    }
    else{
        obj[currObj.winner]= {};
        obj[currObj.winner][currObj.season]=1

    }
    return obj;
},{})

//we need match winner (that means name of team) and after that we have to check 
// how many time this team winner in perticular season 
//then after that we have to add seasons with count increase

console.log(matchesWonPerYear)
const result = JSON.stringify(matchesWonPerYear,'  ', 4)
fs.writeFileSync('../public/2-matches-won-per-team-per-year.json',result)