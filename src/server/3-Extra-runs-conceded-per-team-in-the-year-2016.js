//Extra-runs-conceded-per-team-in-the-year-2016
const fs = require ('fs');
let deliveriesObj = require('../data/reliveries')
let matchesObj = require('../data/matches')

//bewlow line give us a match-ID
const matches_2016_id = matchesObj.filter(pro => pro.season == 2016)
.map(x => x.id)

//then after we get Match-ID compare this id and deliveries id and fretch the data of 2016 deliveries
// for that you have to use filter over deliveries and find over id , if id == deliveres.id => true then these return by Find 
const season_2016_deliveries = deliveriesObj.filter((x)=>{
    return matches_2016_id.find((y) => {
        return y == x.match_id 
    })
})

// below code is for finding the extras run per team 
// for that you have to add key as bolwing teams and value as extra runs and count++ runs once team get as key
const ExtraRuns = season_2016_deliveries.reduce((obj , currObj) => { 
    if (obj[currObj["bowling_team"]]) {
        obj[currObj["bowling_team"]] += Number(currObj["extra_runs"])

    } else {
        obj[currObj["bowling_team"]] = Number(currObj["extra_runs"])
    }
    return obj

},{})
// above code will give a our final result 
// then convert this final to json file

// console.log(ExtraRuns)
const result = JSON.stringify(ExtraRuns,' ','   ')
fs.writeFileSync('../public/3-Extra-runs-conceded-per-team-in-the-year-2016.json',result)

