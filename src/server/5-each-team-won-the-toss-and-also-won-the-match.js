// Find the number of times each team won the toss and also won the match
const fs = require('fs')
let matchesObj = require('../data/matches')

const filtering = matchesObj.filter(
    (fil)  => fil.winner === fil.toss_winner )



const teamsWithtossanswinner = filtering .reduce((obj , curr) => {
    if (obj[curr.winner]) {
        obj[curr.winner] += 1 
    } else {
        obj[curr.winner] = 1
    }

return obj
},{})
console.log(teamsWithtossanswinner)

const result = JSON.stringify(teamsWithtossanswinner,' ','    ' )
fs.writeFileSync('../public/5-each-team-won-the-toss-and-also-won-the-match.json',result)