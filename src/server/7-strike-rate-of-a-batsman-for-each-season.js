// // Find the strike rate of a batsman for each season
const  fs = require('fs')
const matchesObj = require('../data/matches')
let deliveriesObj = require('../data/reliveries')
let finalArray =[ ]
for (let i = 2008 ; i < 2018; i++){
    const matches_id = matchesObj.filter(pro => pro.season == i)
    .map(x => x.id)

    
    const every_season_deliveries = deliveriesObj.filter((x)=>{
        return matches_id.find((y) => {
            return y == x.match_id 
        })
    })
    const playerWithBowlAndRun = every_season_deliveries.reduce((obj , currObj) => { 
        if (obj[currObj.batsman]) {
            obj[currObj.batsman]['runs'] += Number(currObj.batsman_runs)
            obj[currObj.batsman]['balls'] += 1

        } else {
            obj[currObj.batsman] = {}
            obj[currObj.batsman]['runs'] = Number(currObj.batsman_runs)
            obj[currObj.batsman]['balls'] = 1
        }
        return obj

    },{})
    const inArraplayerWithBowlAndRun = Object.entries(playerWithBowlAndRun)
    // console.log(inArraplayerWithBowlAndRun)

    let playerWithEconomy = inArraplayerWithBowlAndRun.map((x) => {
        x[1]=(x[1].runs/x[1].balls)*100;
        return x;
    }).sort((a,b)=> b[1] - a[1]).slice(0,1)

    
    const  finalAns = playerWithEconomy.reduce((obj , curr) => {
    if(false) {

    } else {
        obj[i] = curr[0] + ' | '+Number(curr[1].toFixed(2))
    }
        return obj

    }, { })
    finalArray.push(finalAns)
}
console.log(finalArray)
const result = JSON.stringify(finalArray,'  ', '    ' )
fs.writeFileSync('../public/7-strike-rate-of-a-batsman-for-each-season.json',result)

