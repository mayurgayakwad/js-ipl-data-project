// the-highest-number-of-times-one-player-has-been-dismissed-by-another-player
const fs  = require("fs");
const matchesObj = require('../data/matches')
const deliveriesObj = require('../data/reliveries')

const playerDismissed = deliveriesObj.filter((x) => x.player_dismissed)


let playerDismissCount= playerDismissed.reduce((obj,curr)=>{
    if(obj[curr.player_dismissed+" | "+curr.bowler]){
        obj[curr.player_dismissed+" | "+curr.bowler] += 1;
    }
    else{
        obj[curr.player_dismissed+" | "+curr.bowler] = 1;
    }
    return obj;
},{});

const finalAns = Object.entries(playerDismissCount).sort((a,b) => b[1] - a[1]).slice(0,2)
console.log(finalAns)

const obj = {}
obj[finalAns[0][0]] = finalAns[0][1]
obj[finalAns[1][0]] = finalAns[1][1]
console.log(obj)
const result = JSON.stringify(obj,'  ', '     ')
fs.writeFileSync('../public/8-the-highest-number-of-times-one-player-has-been-dismissed-by-another-player.json',result)